<?php

/**
 * @copyright Copyright (C) Ibexa AS. All rights reserved.
 * @license For full copyright and license information view LICENSE file distributed with this source code.
 */
declare(strict_types=1);

namespace Contextualcode\Bundle\Connector\Brandfolder\DependencyInjection\CompilerPass;

use Contextualcode\Bundle\Connector\Brandfolder\DependencyInjection\Configuration\Parser\Configuration;
use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;

final class DefaultTransformationsPass implements CompilerPassInterface
{
    public function process(ContainerBuilder $container)
    {
        $setUpConfiguration = $container->getParameter('ibexa.connector.brandfolder.image.variations');

        $container->setParameter(
            'ibexa.connector.brandfolder.image.variations',
            array_merge(
                [
                    'reference' => [
                        Configuration::IMAGE_FORMAT => 'jpg',
                        Configuration::QUALITY => 80,
                    ],
                    'tiny' => [
                        Configuration::IMAGE_FORMAT => 'jpg',
                        Configuration::WIDTH => 30,
                        Configuration::QUALITY => 80,
                        Configuration::FIT_MODE => 'max',
                    ],
                    'small' => [
                        Configuration::IMAGE_FORMAT => 'jpg',
                        Configuration::WIDTH => 100,
                        Configuration::QUALITY => 80,
                        Configuration::FIT_MODE => 'max',
                    ],
                    'medium' => [
                        Configuration::IMAGE_FORMAT => 'jpg',
                        Configuration::WIDTH => 200,
                        Configuration::QUALITY => 80,
                        Configuration::FIT_MODE => 'max',
                    ],
                    'large' => [
                        Configuration::IMAGE_FORMAT => 'jpg',
                        Configuration::WIDTH => 300,
                        Configuration::FIT_MODE => 'max',
                    ],
                ],
                $setUpConfiguration,
            )
        );
    }
}
