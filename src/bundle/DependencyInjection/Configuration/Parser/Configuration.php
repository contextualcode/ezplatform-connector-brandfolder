<?php

/**
 * @copyright Copyright (C) Ibexa AS. All rights reserved.
 * @license For full copyright and license information view LICENSE file distributed with this source code.
 */
declare(strict_types=1);

namespace Contextualcode\Bundle\Connector\Brandfolder\DependencyInjection\Configuration\Parser;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

final class Configuration implements ConfigurationInterface
{
    private const TREE_ROOT = 'dam_brandfolder';
    public const WIDTH = 'width';
    public const HEIGHT = 'height';
    public const CROP_MODE = 'crop';
    public const IMAGE_FORMAT = 'fm';
    public const AUTO_CONFIGURATION = 'auto';
    public const QUALITY = 'q';
    public const FIT_MODE = 'fit';
    public const DEVICE_PIXEL_RATIO = 'dpr';

    public function getConfigTreeBuilder(): TreeBuilder
    {
        $treeBuilder = new TreeBuilder(self::TREE_ROOT);

        $rootNode = $treeBuilder->getRootNode();
        $rootNode
            ->children()
                ->scalarNode('application_id')
                    ->isRequired()
                ->end()
                ->scalarNode('secret')
                    ->defaultNull()
                ->end()
                ->arrayNode('collections')
                    ->isRequired()
                    ->scalarPrototype()->end()
                ->end()
                ->arrayNode('variations')
                    ->ignoreExtraKeys(false)
                    ->arrayPrototype()
                        ->children()
                        ->integerNode(self::WIDTH)
                            ->info('width')
                        ->end()
                        ->integerNode(self::HEIGHT)
                            ->info('height')
                        ->end()
                    ->end()
                ->end()
            ->end();

        return $treeBuilder;
    }
}
