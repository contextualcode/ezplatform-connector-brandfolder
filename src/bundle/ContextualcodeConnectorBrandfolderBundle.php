<?php

/**
 * @copyright Copyright (C) Ibexa AS. All rights reserved.
 * @license For full copyright and license information view LICENSE file distributed with this source code.
 */
declare(strict_types=1);

namespace Contextualcode\Bundle\Connector\Brandfolder;

use Contextualcode\Bundle\Connector\Brandfolder\DependencyInjection\CompilerPass\DefaultTransformationsPass;
use Contextualcode\Bundle\Connector\Brandfolder\DependencyInjection\ContextualcodeConnectorBrandfolderExtension;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\HttpKernel\Bundle\Bundle;

final class ContextualcodeConnectorBrandfolderBundle extends Bundle
{
    public function getContainerExtension()
    {
        return new ContextualcodeConnectorBrandfolderExtension();
    }

    public function build(ContainerBuilder $container)
    {
        /** @var \eZ\Bundle\EzPublishCoreBundle\DependencyInjection\EzPublishCoreExtension $core */
        $core = $container->getExtension('ezpublish');

        $core->addDefaultSettings(__DIR__ . '/Resources/config', ['default_settings.yaml']);
        //$container->addCompilerPass(new DefaultTransformationsPass());
    }
}
