<?php

/**
 * @copyright Copyright (C) Ibexa AS. All rights reserved.
 * @license For full copyright and license information view LICENSE file distributed with this source code.
 */
declare(strict_types=1);

namespace Contextualcode\Connector\Brandfolder;

use Brandfolder\Brandfolder;
use Ibexa\Platform\Contracts\Connector\Dam\Search\AssetSearchResult;
use Ibexa\Platform\Contracts\Connector\Dam\AssetCollection;
use Contextualcode\Connector\Brandfolder\Asset\Photo;

class BrandfolderClient
{

    /** @var \Brandfolder\Brandfolder */
    private $inner_client;

    public function __construct(
        string $applicationId,
        ?string $secret = null
    ) {
        $this->inner_client = new Brandfolder($secret, $applicationId);
    }

    public function getSections() {
        return $this->inner_client->listSectionsInBrandfolder();
    }

    public function photos(
        string $searchPhrase,
        int $page,
        int $limit
    ) {
        $results_object = $this->inner_client->listAssets(array('search'=>$searchPhrase, 'page' => $page, 'per' => $limit, 'fields' => 'cdn_url', 'include' => 'attachments'));
        $results_count = $results_object->meta->total_count;
        $results_collection = new AssetCollection($results_object->data);
        return new AssetSearchResult($results_count, $results_collection);
    }

    public function find(
        string $id
    ) {
        list($type, $dam_id) = explode('_', $id);
        if ($type == 'asset') {
            $result = $this->inner_client->fetchAsset($dam_id, array('fields' => 'cdn_url'));
        } else {
            $result = $this->inner_client->fetchAttachment($dam_id, array('fields' => 'cdn_url'));
        }
        return new Photo($result);
    }

    public function download(
        string $id
    ): void {
        $asset = $this->find($id);
        $asset->download();
    }
}
