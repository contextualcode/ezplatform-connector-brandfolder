<?php

/**
 * @copyright Copyright (C) Ibexa AS. All rights reserved.
 * @license For full copyright and license information view LICENSE file distributed with this source code.
 */
declare(strict_types=1);

namespace Contextualcode\Connector\Brandfolder\Handler;

use Contextualcode\Connector\Brandfolder\BrandfolderClient;
use Ibexa\Platform\Contracts\Connector\Dam\Asset;
use Ibexa\Platform\Contracts\Connector\Dam\AssetCollection;
use Ibexa\Platform\Contracts\Connector\Dam\AssetIdentifier;
use Ibexa\Platform\Contracts\Connector\Dam\AssetMetadata;
use Ibexa\Platform\Contracts\Connector\Dam\AssetSource;
use Ibexa\Platform\Contracts\Connector\Dam\AssetUri;
use Ibexa\Platform\Contracts\Connector\Dam\Handler\Handler as HandlerInterface;
use Ibexa\Platform\Contracts\Connector\Dam\Search\AssetSearchResult;
use Ibexa\Platform\Contracts\Connector\Dam\Search\Query;

final class Handler implements HandlerInterface
{
    /** @var \Contextualcode\Connector\Brandfolder\BrandfolderClient */
    private $client;

    public function __construct(BrandfolderClient $client)
    {
        $this->client = $client;
    }

    public function fetchAsset(string $id): Asset
    {
        $photo = $this->client->find($id);

        return new Asset(
            new AssetIdentifier((string)$photo->id),
            new AssetSource('brandfolder'),
            new AssetUri($photo->urls['cdn_url']),
            new AssetMetadata([
                'alternativeText' => $photo->description ?? '',
                'width' => $photo->width,
                'height' => $photo->height,
                'created_at' => $photo->created_at,
                'updated_at' => $photo->updated_at,
                'author' => $photo->photographer()->name,
                'author_link_html' => $photo->photographer()->links['html'],
            ])
        );
    }

    /**
     * @param \Contextualcode\Connector\Brandfolder\Query\Query $query
     */
    public function search(
        Query $query,
        int $offset = 0,
        int $limit = 20
    ): AssetSearchResult {
        $page = ($offset / $limit) + 1;
        $pageResult = $this->client->photos(
            $query->getPhrase(),
            $page,
            $limit,
            ''
        );

        $results = $pageResult->getResults();

        $assets = [];
        foreach ($results as $result) {
            if ($result->attachments && count($result->attachments) > 0) {
                foreach ($result->attachments as $id => $attachment) {
                    $assets[] = new Asset(
                        new AssetIdentifier('attachment_'.$id),
                        new AssetSource('brandfolder'),
                        new AssetUri(urldecode($attachment->thumbnail_url)),
                        new AssetMetadata([
                            'alternativeText' => $attachment->filename,
                            'author' => '',
                            'author_link_html' => '',
                        ])
                    );
                }
            } else {
                $assets[] = new Asset(
                    new AssetIdentifier('asset_'.$result->id),
                    new AssetSource('brandfolder'),
                    new AssetUri(urldecode($result->attributes->thumbnail_url)),
                    new AssetMetadata([
                        'alternativeText' => $result->attributes->description,
                        'author' => '',
                        'author_link_html' => '',
                    ])
                );
            }
        }

        return new AssetSearchResult(
            $pageResult->getTotalCount(),
            new AssetCollection($assets)
        );
    }
}
