<?php

/**
 * @copyright Copyright (C) Ibexa AS. All rights reserved.
 * @license For full copyright and license information view LICENSE file distributed with this source code.
 */
declare(strict_types=1);

namespace Contextualcode\Connector\Brandfolder\Asset;

class Photo
{

    public $id;
    public $name;
    public $description;
    public $width;
    public $height;
    public $created_at;
    public $updated_at;
    public $urls;

    public function __construct(
        $stdClass
    ) {
        $attributes = $stdClass->data->attributes;
        $this->id = $stdClass->data->id;
        $this->name = property_exists($attributes, 'name') ? $attributes->name : $attributes->filename;
        $this->description = property_exists($attributes, 'description') ? $attributes->description : '';
        $this->width = '100%';
        $this->height = '100%';
        $this->created_at = '';
        $this->updated_at = '';
        $this->urls = array('raw'=> $attributes->thumbnail_url, 'cdn_url' => $attributes->cdn_url ?: '' );
    }

    public function photographer(
    ) {
        $photographer = new \stdClass();
        $photographer->name = 'Anon';
        $photographer->links = array('html' => '#');
        return $photographer;
    }

    function download() {
        return $this->urls['cdn_url'];
    }
}
