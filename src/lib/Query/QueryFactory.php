<?php

/**
 * @copyright Copyright (C) Ibexa AS. All rights reserved.
 * @license For full copyright and license information view LICENSE file distributed with this source code.
 */
declare(strict_types=1);

namespace Contextualcode\Connector\Brandfolder\Query;

use Ibexa\Platform\Contracts\Connector\Dam\Search\Query as BaseQuery;
use Ibexa\Platform\Contracts\Connector\Dam\Search\QueryFactory as QueryFactoryInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

class QueryFactory implements QueryFactoryInterface
{
    public function buildFromRequest(Request $request): BaseQuery
    {
        if (null === $request->get('query')) {
            throw new BadRequestHttpException('Request do not contain all required arguments');
        }

        $section = $request->get('section');

        if ($section === '') {
            $section = null;
        }
        if (null !== $section
            && !\in_array($request->get('section'), ['landscape', 'portrait', 'squarish'])
        ) {
            throw new BadRequestHttpException('Available section options are: landscape, portrait, squarish');
        }

        return new Query(
            $request->get('query'),
            $section
        );
    }
}
