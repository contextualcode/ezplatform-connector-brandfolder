<?php

/**
 * @copyright Copyright (C) Ibexa AS. All rights reserved.
 * @license For full copyright and license information view LICENSE file distributed with this source code.
 */
declare(strict_types=1);

namespace Contextualcode\Connector\Brandfolder\Variation;

use Ibexa\Platform\Contracts\Connector\Dam\Variation\Transformation;
use Ibexa\Platform\Contracts\Connector\Dam\Variation\TransformationFactory as TransformationFactoryInterface;

final class TransformationFactory implements TransformationFactoryInterface
{
    private $transformationsProvider;

    public function __construct(TransformationsProvider $transformationsProvider)
    {
        $this->transformationsProvider = $transformationsProvider;
    }

    public function build(?string $transformationName = null, array $transformationParameters = []): Transformation
    {
        if ($transformationName) {
            return $this->transformationsProvider->getTransformation($transformationName);
        }

        return new Transformation(null, $transformationParameters);
    }

    public function buildAll(): iterable
    {
        return $this->transformationsProvider->getAvailableTransformations();
    }
}
