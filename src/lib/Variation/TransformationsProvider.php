<?php

/**
 * @copyright Copyright (C) Ibexa AS. All rights reserved.
 * @license For full copyright and license information view LICENSE file distributed with this source code.
 */
declare(strict_types=1);

namespace Contextualcode\Connector\Brandfolder\Variation;

use Ibexa\Platform\Contracts\Connector\Dam\Variation\Transformation;

class TransformationsProvider
{

    private $availableTransformations = [];

    public function __construct(array $transformationsConfiguration)
    {
        foreach ($transformationsConfiguration as $name => $parameters) {
            $this->availableTransformations[$name] = new Transformation(
                $name,
                $parameters
            );
        }
    }

    public function getTransformation(string $name): Transformation
    {
        return $this->availableTransformations[$name] ?? new Transformation($name, []);
    }

    public function getAvailableTransformations(): array
    {
        return $this->availableTransformations;
    }
}
