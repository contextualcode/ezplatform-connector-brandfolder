<?php

/**
 * @copyright Copyright (C) Ibexa AS. All rights reserved.
 * @license For full copyright and license information view LICENSE file distributed with this source code.
 */
declare(strict_types=1);

namespace Contextualcode\Connector\Brandfolder\Tab;

use Ibexa\Platform\Connector\Dam\View\Search\Tab\GenericSearchTab;
use Symfony\Component\Form\FormInterface;
use Contextualcode\Connector\Brandfolder\BrandfolderClient;
use eZ\Publish\Core\MVC\ConfigResolverInterface;
use Symfony\Component\Form\FormFactory;
use Symfony\Contracts\Translation\TranslatorInterface;
use Twig\Environment;

final class SearchTab extends GenericSearchTab
{

    /** @var \Contextualcode\Connector\Brandfolder\BrandfolderClient */
    protected $client;

    public function __construct(
        Environment $twig,
        TranslatorInterface $translator,
        string $identifier,
        string $source,
        string $name,
        string $searchFormType,
        FormFactory $formFactory,
        ConfigResolverInterface $configResolver,
        BrandfolderClient $client
    ) {
        parent::__construct($twig, $translator, $identifier, $source, $name, $searchFormType, $formFactory, $configResolver);
        $this->cient = $client;
    }

    public function renderView(array $parameters): string
    {
        return $this->twig->render('@ezdesign/search/brandfolder_form.html.twig', [
            'form' => $this->getForm()->createView(),
            'source' => $this->identifier,
        ]);
    }

    protected function getForm(): FormInterface
    {
        $sections = [];
        foreach ($this->cient->getSections()->data as $section) {
            $sections[$section->attributes->name] = $section->attributes->name;
        }
        return $this->formFactory->createNamed(
            $this->getIdentifier() . '-form',
            $this->searchFormType,
            [                
                'sections' => $sections,
            ],
            [
                'csrf_protection' => false,
            ]
        );
    }
}
