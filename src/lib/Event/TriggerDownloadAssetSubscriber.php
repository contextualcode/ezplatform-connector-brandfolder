<?php

/**
 * @copyright Copyright (C) Ibexa AS. All rights reserved.
 * @license For full copyright and license information view LICENSE file distributed with this source code.
 */
declare(strict_types=1);

namespace Contextualcode\Connector\Brandfolder\Event;

use Ibexa\Platform\Connector\Dam\Event\PublishAssetEvent;
use Contextualcode\Connector\Brandfolder\BrandfolderClient;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class TriggerDownloadAssetSubscriber implements EventSubscriberInterface
{
    /** @var \Contextualcode\Connector\Brandfolder\BrandfolderClient */
    private $client;

    public function __construct(BrandfolderClient $client)
    {
        $this->client = $client;
    }

    public static function getSubscribedEvents()
    {
        return [
            PublishAssetEvent::class => 'markAssetAsUsed',
        ];
    }

    public function markAssetAsUsed(PublishAssetEvent $event): void
    {
        if ($event->getAssetSource()->getSourceIdentifier() !== 'brandfolder') {
            return;
        }

        $this->client->download($event->getAssetIdentifier()->getId());
    }
}
