<?php

namespace Contextualcode\Connector\Brandfolder\Twig;

use Novactive\Bundle\eZSEOBundle\Twig\NovaeZSEOExtension as BaseNovaeZSEOExtension;
use Twig\Extension\GlobalsInterface;
use Contextualcode\Connector\Brandfolder\Core\MetaNameSchema;
use eZ\Publish\Core\MVC\ConfigResolverInterface;
use eZ\Publish\API\Repository\Repository;
use eZ\Publish\Core\MVC\Symfony\Locale\LocaleConverterInterface as LocaleConverter;
use Novactive\Bundle\eZSEOBundle\Core\CustomFallbackInterface;

class NovaeZSEOExtension extends BaseNovaeZSEOExtension implements GlobalsInterface
{
    /**
     * The eZ Publish object name pattern service (extended).
     *
     * @var MetaNameSchema
     */
    protected $metaNameSchema;

    /**
     * ConfigResolver useful to get the config aware of siteaccess.
     *
     * @var ConfigResolverInterface
     */
    protected $configResolver;

    /**
     * The eZ Publish API.
     *
     * @var Repository
     */
    protected $eZRepository;

    /**
     * Locale Converter.
     *
     * @var LocaleConverter
     */
    protected $localeConverter;

    /**
     * CustomFallBack Service.
     *
     * @var CustomFallbackInterface
     */
    protected $customFallBackService;

    public function __construct(
        Repository $repository,
        MetaNameSchema $nameSchema,
        ConfigResolverInterface $configResolver,
        LocaleConverter $localeConverter
    ) {
        $this->metaNameSchema = $nameSchema;
        $this->eZRepository = $repository;
        $this->configResolver = $configResolver;
        $this->localeConverter = $localeConverter;
    }

}
